{{ dl.packages_url }}/bleach-1.5.0-py2.py3-none-any.whl
{{ dl.packages_url }}/enum34-1.1.6-py3-none-any.whl
{{ dl.packages_url }}/html5lib-0.9999999.tar.gz
{{ dl.packages_url }}/Keras-2.1.2-py2.py3-none-any.whl
{{ dl.packages_url }}/Markdown-2.6.10.zip
{{ dl.packages_url }}/nltk-3.2.5.tar.gz
{{ dl.packages_url }}/numpy-1.13.3-cp36-cp36m-manylinux1_x86_64.whl
{{ dl.packages_url }}/protobuf-3.5.1-cp36-cp36m-manylinux1_x86_64.whl
{{ dl.packages_url }}/PyYAML-3.12.tar.gz
{{ dl.packages_url }}/scipy-1.0.0-cp36-cp36m-manylinux1_x86_64.whl
{{ dl.packages_url }}/setuptools-38.2.5-py2.py3-none-any.whl
{{ dl.packages_url }}/six-1.11.0-py2.py3-none-any.whl
{{ dl.packages_url }}/tensorflow_gpu-1.4.1-cp36-cp36m-manylinux1_x86_64.whl
{{ dl.packages_url }}/tensorflow_tensorboard-0.4.0rc3-py3-none-any.whl
{{ dl.packages_url }}/Werkzeug-0.14.1-py2.py3-none-any.whl
{{ dl.packages_url }}/wheel-0.30.0-py2.py3-none-any.whl