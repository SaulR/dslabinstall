#!/usr/bin/env bash

LOG_PATH={{ logPath }}/jupyterhub_`date +%Y-%m-%d`.log
APPNAME=jupyterhub
mkdir -p `dirname $LOG_PATH`
touch $LOG_PATH
echo "[JUPYTERHUB] `date +'%Y-%m-%d %H:%M:%S'` Arrancando Jupyterhub -[ START ] Log disponible en: ${LOG_PATH}"

eval "$@" 2>&1 | tee ${LOG_PATH}
#eval "$@" 2>&1 | awk '{ print strftime("[JUPYTERHUB] %Y-%m-%d %H:%M:%S DataLab log -"), $0 }' | tee ${LOG_PATH}