# Bankia Datalab

The objective here is to provide a jupyterhub in a CentOS-7 with not connection to internet.

Components: JupyterHub

Kernels: python2, python3, pyspark and Toree - Scala

## In order to reproduce the env, a VM virtual box with centOS 7

A VM virtual box file is provided.

This VM has two mounted directories, sf_sharedFolder and sf_bankia_dataLab.
These are shared folders from the host to the guest OS. This allows us to move files between both environments, it can be helpful in case we need to get something from the internet and make it available on the guest OS.

* __sharedFolder__ contains all the files and packages necessary for the installation.

* __bankia\_dataLab__ cointains the ansible scripts and configuration files.

Both should be under /media folder.


##Preparing the environment 

It is necessary to prepare the VM in order to simulate the conditions we will have once we are working on the Bankia server. No internet connection is available.

1. Unzip the content of dataLabVM. Two folders and one file .ova should be available.
2. Import the VM. Be sure to point the shared folders to the path were you have unziped the archive.
1. Boot the VM. Login with root:123456. Make sure the internet connection is on.
2. Open a terminal session and change directory to sf_bankia_dataLab. 
3. Run the script __initialTasksOnline.sh__. This script will perform the following commands:

_(all these commands are performed online)_

```
yum install epel-release -y
```
```
yum install npm -y
```
```
npm install http-server -g
```
```
yum install ansible -y
```
```
http-server /media/sf_shareFolder
```

##Installation process:

From here everything should be installed with ansible script.
Be sure to disable your network connection.

On a terminal window, issue the ansible command to run the playbook:
```
ansible-playbook -i inv.txt /media/sf_bankia_dataLab/provisioning/dataLab_playbook.yml
```

Your installation should finish with success. If not, check the Known issues section in order to find a possible solution to your issue.

###Known issues

The ansible script execution should fail on the step where we install the __configurable-http-proxy.cache.tar.gz__ due to some metadata file being necessary to download from the internet. 
To overcome this problem you can enable/disable your network connection.
A complete offline solution needs to be achieved.


Some changes were made in order to add the serives jupyterhub to start on boot. It is also possible to start, stop and status the service _```e.g.(systemctl start jupyterhub)```_

Authentication on jupyterhub is only possible with the user jupyterhub user. 
Be sure to change the passwd before loging in. _(``` passwd jupyterhub ```)_ 




